proftpd-mod-vroot (0.9.9-1) UNRELEASED; urgency=medium

  * New upstream release.

 -- Hilmar Preusse <hille42@web.de>  Sun, 05 Sep 2021 22:58:08 +0200

proftpd-mod-vroot (0.9.8-4) unstable; urgency=medium

  [ Hilmar Preusse ]
  * Remove two header files not needed for build
    (debian/proftp_code/buildstamp.h debian/proftp_code/config.h)
  * Do not rebuild configure script (--without autoreconf).
    Can now switch to Debhelper = 13.
  * Remove configure script from .gitignore.
  * Remove override_dh_auto_build
    - dh $@ ... --max-parallel=1 (just a precaution).
  * d/rules: add "export DEB_BUILD_MAINT_OPTIONS = hardening=+all"
    ...does not seem to be effective.
  * lintian
    I: proftpd-mod-vroot source: quilt-patch-missing-description(...)
    P: proftpd-mod-vroot source: silent-on-rules-requiring-root
    P: proftpd-mod-vroot source: unversioned-copyright-format-uri
    I: Trailing-whitespace

  [ Francesco Paolo Lovergine ]
  * Removed past relationships with a now transitional -basic package.
  * Policy bumped to 4.5.1, no changes.

 -- Francesco Paolo Lovergine <frankie@debian.org>  Thu, 24 Dec 2020 18:10:01 +0100

proftpd-mod-vroot (0.9.8-2) unstable; urgency=medium

  * Completely rework of build system. We can't use prxs to build
    modules, if they consist of more than one source code file.
    This Closes: #951916 .

 -- Hilmar Preusse <hille42@web.de>  Thu, 13 Aug 2020 19:41:19 +0200

proftpd-mod-vroot (0.9.8-1) unstable; urgency=medium

  * New upstream release snapshot
    - Is compatible to 1.3.6 API -> two patches gone.
  * Remove Fabrizio Regalli <fabreg@fabreg.it> from uploaders.
    (Closes: #761865)
    - add me to Uploaders.
  * Fix Maintainers E-Mail address.
  * Fix Vcs-Git & Vcs-Browser URL.
  * Update to Standards version 4.5.0 (no changes needed).

 -- Hilmar Preusse <hille42@web.de>  Wed, 19 Feb 2020 23:36:40 +0100

proftpd-mod-vroot (0.9.4-2) unstable; urgency=medium

  [Hilmar Preuße]

  * 2 patches from upstream to make package compatible to proftp 1.3.6.
    (Closes: #892371)

 -- Francesco Paolo Lovergine <frankie@debian.org>  Wed, 14 Mar 2018 11:41:13 +0100

proftpd-mod-vroot (0.9.4-1) unstable; urgency=medium

  [ Mahyuddin Susanto ]
  * Team upload.
  * New Upstream release 0.9.3.
    -> It has a fix for erroneous function vroot_lookup_path (Closes: #759936)
  * Bump Standard Version to 3.9.5.
  * d/changelog: fix previous release pocket, already uploaded but
    still unreleased.
  * d/watch: upstream moved to github (Closes: #732660)
  * Rebuild against latest proftpd (Closes: #715569)

  [Hilmar Preuße]
  * d/control: Remove "DM-Upload-Allowed" field
  * d/rules: add --without python-support to dh call
  * d/rules: overhaul clean target to make it work

  [ Francesco Paolo Lovergine ]
  * New upstream release.
  * Policy bumped to 3.9.8.

 -- Francesco Paolo Lovergine <frankie@debian.org>  Sat, 24 Dec 2016 19:42:04 +0100

proftpd-mod-vroot (0.9.2-2) unstable; urgency=low

  * Fixed spelling-error-in-description lintian message
  * Update d/copyright to latest .174 DEP5 revision
  * Fixed timewarp-standards-version lintian message

 -- Fabrizio Regalli <fabreg@fabreg.it>  Thu, 29 Sep 2011 15:24:55 +0200

proftpd-mod-vroot (0.9.2-1) unstable; urgency=low

  * Initial Release. (closes: #618765)
  * Added doc-base
  * Added dh_clean to d/rules

 -- Fabrizio Regalli <fabreg@fabreg.it>  Thu, 15 Mar 2011 18:14:35 +0100
